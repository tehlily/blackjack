const MDCTextField = mdc.textField.MDCTextField;
const foos = [].map.call(
    document.querySelectorAll(".mdc-text-field"),
    function (el) {
        return new MDCTextField(el);
    }
);

console.log(document.forms);

const signUpForm = document.forms[0];

const ageInput = signUpForm[0];
const dobInput = signUpForm[1];
const fullNameInput = signUpForm[2];
const usernameInput = signUpForm[3];
const enterPasswordInput = signUpForm[4];
const confirmPasswordInput = signUpForm[5];
const legalCheckbox = signUpForm[6];
const tosCheckbox = signUpForm[7];
const signUpButton = signUpForm[8];

var boxesChecked = false;
var legalChecked = false;
var termsChecked = false;

const signUpFormInputs = [
    ageInput,
    dobInput,
    fullNameInput,
    usernameInput,
    enterPasswordInput,
    confirmPasswordInput
]

const signUpFormCheckboxes = [
    legalCheckbox,
    tosCheckbox
]

function logAllInfo() {
    console.log("Full Name: " + fullNameInput.value);
    console.log("Username: " + usernameInput.value);
    console.log("Age: " + ageInput.value);
    console.log("Birth Date: " + dobInput.value);

    console.log("Enter Password: " + enterPasswordInput.value);
    console.log("Confirm Password: " + confirmPasswordInput.value);
}

function checkCheckboxes() {
    // let legalChecked = false;
    // let termsChecked = false;
    if(signUpForm[6].checked) {
        console.log("The user has checked the legal checkbox");
        legalChecked = true;
    } else {
        console.log("The user has not checked the legal checkbox");
        legalChecked = false;
    }
    if(signUpForm[7].checked) {
        console.log("The user has checked the terms checkbox");
        termsChecked = true;
    } else {
        console.log("The user has not checked the terms checkbox");
        termsChecked = false;
    }

}

function checkLegalbox() {
    if(signUpForm[6].checked) {
        console.log("The user has checked the legal checkbox");
        legalChecked = true;
    } else {
        console.log("The user has not checked the legal checkbox");
        legalChecked = false;
    }
}

function checkTermsbox() {
    if(signUpForm[7].checked) {
        console.log("The user has checked the terms checkbox");
        termsChecked = true;
    } else {
        console.log("The user has not checked the terms checkbox");
        termsChecked = false;
    }
}

function isNotEmpty(input) {
    return input.value !== "";
}

function noEmptyFields() {
    return signUpFormInputs.every(isNotEmpty);
}


function verifyInfo() {
    event.preventDefault();

    
    checkCheckboxes();

    if(legalChecked && termsChecked) {
        boxesChecked = true;
    }

    if(noEmptyFields() && boxesChecked) {
        if(enterPasswordInput.value === confirmPasswordInput.value) {
            if(ageInput.value > 12){
                console.log("The user is eligible")
            }else {
                console.log("The user is ineligible")
                
            }
        } else {
            console.log("The user is ineligible")
        }
    } else {
        console.log("The user is ineligible")
    }
    logAllInfo();

}

document.getElementById('signUpButton').addEventListener("click", verifyInfo);
