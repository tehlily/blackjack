// import {MDCList} from "@material/list";
// const list = MDCList.attachTo(document.querySelector('.mdc-list'));
// list.wrapFocus = true;

// import {MDCDrawer} from "@material/drawer";
// const drawer = MDCDrawer.attachTo(document.querySelector('.mdc-drawer'));

const listEl = document.querySelector('.mdc-drawer .mdc-list');
const mainContentEl = document.querySelector('.main-content');

// listEl.addEventListener('click', (event) => {
//   drawer.open = false;
// });

document.body.addEventListener('MDCDrawer:closed', () => {
  mainContentEl.querySelector('input, button').focus();
});

// import {MDCTextField} from '@material/textfield';

// const textField = new MDCTextField(document.querySelector('.mdc-text-field'));

