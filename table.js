// import {MDCTooltip} from '@material/tooltip';
// const tooltip = new MDCTooltip(document.querySelector('.md-tooltip'));

// tooltip = "this is a tooltip";

const suits = ["SPADES", "HEARTS", "DIAMONDS", "CLUBS"];
const ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "JACK", "QUEEN", "KING"];

const mapRanksToWords = { "": "Mystery", "2": "Two", "3": "Three", "4": "Four", "5": "Five", "6": "Six", "7": "Seven", "8": "Eight", "9": "Nine", "10": "Ten", "JACK": "Jack", "QUEEN": "Queen", "KING": "King", "ACE": "Ace"};
const mapSuitsToWords = {"": "Mystery", "SPADES": "Spades", "HEARTS": "Hearts", "DIAMONDS": "Diamonds", "CLUBS": "Clubs"}
const mapRanksToValues = {"Ace": "11/1", "Two": "2", "Three": "3", "Four": "4", "Five": "5", "Six": "6", "6": "6", "Seven": "7", "Eight": "8", "Nine": "9", "Ten": "10", "10": "10", "Jack": "10", "Queen": "10", "King": "10", "Mystery": "?", "Face Down": "?", "ACE": "11/1", "JACK": "10"}

function Card(suit, rank, imgSource) {
    this.suit = suit;
    // console.log(this.suit);
    this.rank = rank;
    // console.log(this.rank);
    this.imgSource = imgSource;
}

var firstFour = [];
var mysteryCard;

var cardsDealtByIndex = [];
var currentDeckId;
// var currentDeckId = getDeck();

var player1 = Card("0", "0", "");
// console.log(player1.suit);
var player2 = Card("0", "0", "");
var dealer1 = Card("0", "0", "");
var dealer2 = Card("0", "0", "");

const hitButton = document.getElementById("hitMe");

const dealButton = document.getElementById("deal");

let deckId;
getShoe((data) => {
    deckId = data.deck_id;
});
var deckInfo;

const downCard = new Image();
downCard.src = "https://previews.123rf.com/images/rlmf/rlmf1512/rlmf151200171/49319432-playing-cards-back.jpg";
// downCard.src = "Images/singleCardBack.png";
downCard.setAttribute('alt', '?');
downCard.setAttribute('height', '100px');

let timeoutID = null;

function callTimeout() {
    timeoutID = setTimeout(() => {
        console.log("player timeout");
        addDealerValues();
    }, 10000);
}

function delayTimeout() {
    if(timeoutID != null){
        clearTimeout(timeoutID);
    }
    callTimeout();
}
function dealDown(elementID) {
    let placement = document.getElementById(elementID);
    insertTxt = "<li data-blackJACK-value='?' id='card'><img src='https://previews.123rf.com/images/rlmf/rlmf1512/rlmf151200171/49319432-playing-cards-back.jpg' alt='?' height='100px'/></li>";  
    
    placement.innerHTML +=insertTxt;
}

function suitToWord(suit) {
    return mapSuitsToWords[suit];
}

function rankToValue(rank) {
    // console.log(mapRanksToValues[rank]);
    return mapRanksToValues[rank];
}

function rankToWord(rank) {
    return mapRanksToWords[rank];
}

function clearTable() { 
    let blankSlate = "";
    let card1 = document.getElementById("playerCard1");
    let card2 = document.getElementById("playerCard2");
    let card3 = document.getElementById("dealerCard1");
    let card4 = document.getElementById("dealerCard2");
    const playersCardList = document.querySelector("#playersHand");
    playersCardList.firstChild;
    while (playersCardList.firstChild) {
        playersCardList.removeChild(playersCardList.firstChild);
    }
    const dealersCardList = document.querySelector("#dealersHand");
    dealersCardList.firstChild;
    while (dealersCardList.firstChild) {
        dealersCardList.removeChild(dealersCardList.firstChild);
    }

    card1.innerHTML = blankSlate;
    card2.innerHTML = blankSlate;
    card3.innerHTML = blankSlate;
    card4.innerHTML = blankSlate;
    
    return;
}

function displayCard(card, elementID) {
    
    card.suit = suitToWord(card.suit);
    card.rank = rankToWord(card.rank);

    // console.log(card.rank);
    if (elementID == 'dealerCard2') {
        dealDown(elementID);
    } else {
        let placement = document.getElementById(elementID);
        let srcStart = "<li data-blackJACK-value='";

        let rankNum = rankToValue(card.rank);
        // console.log(rankNum);
        let lengthRank = rankNum.length;
        if(lengthRank > 2 && rankNum != "11/1") {
            rankNum = card.rank;
        }
        console.log(rankNum);

        insertTxt = srcStart.concat(rankNum, "' id='card'><img src='", card.imgSource, "' alt='", card.rank, " of ", card.suit, "' height='100px'/></li>"); 
        
        placement.innerHTML +=insertTxt;
    }

}

function displayHit(card) {
    
    card.suit = suitToWord(card.suit);
    card.rank = rankToWord(card.rank);
    cardValue = rankToValue(card.rank);

    altTxt = card.rank + " of " + card.suit;

    const newCardDealt = document.createElement("li");
    newCardDealt.setAttribute("data-blackJACK-value", cardValue);
    newCardDealt.setAttribute("id", "card");

    const imgSpan = document.createElement("img");
    imgSpan.setAttribute("src", card.imgSource);
    imgSpan.setAttribute("alt", altTxt);
    imgSpan.setAttribute("height", "100px");
    newCardDealt.appendChild(imgSpan);

    // const textSpan = document.createElement("span");
    // textSpan.innerText = card.rank + " of " + card.suit;
    // newCardDealt.appendChild(textSpan);

    document.querySelector("#playersHand").appendChild(newCardDealt);
    // document.getElementById("addCard").innerHTML +=insertTxt;
    
    // let insertTxt = "<li data-blackJACK-value='" + cardValue + "' id='card'><img src='" + card.imgSource + "alt = " + card.rank + " of " + card.suit + "' height = '100px'/><li>";

    // if(cardValue == "11/1") {
    //     cardValue = "Ace"
    // } 

    // insertTxt = srcStart.concat(cardValue, card.suit, ".png' alt=;", card.rank, " of ", card.suit, "' height='100px'/><li>");

    // document.getElementById("addCard").innerHTML +=insertTxt;
    console.log(addPlayerValues())
}

function dealRandomCard() {
    let cardToDisplay = getOneCard();
    console.log(cardToDisplay);
    displayHit(cardToDisplay);
}

function checkRemaining(id, callback) {
    fetch(`https://www.deckofcardsapi.com/api/deck/${id}`)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        })
        .then(data => {
            callback(data);
        })
}

function isDeckEmpty() {
     console.log(deckId);
    checkRemaining(deckId, function (numRemaining) {
        // console.log(numRemaining.remaining);
        deckInfo = numRemaining;
        if(numRemaining.remaining < 1) {
            return true;
        } else {
            return false;
        }
    })
}

function drawOne(id, callback) {
    // console.log(id);
    fetch(`https://www.deckofcardsapi.com/api/deck/${id}/draw?count=1`)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        })
        .then(data => {
            callback(data);
        })
}

function getOneCard() {
    
        drawOne(deckId, function (cardToDeal) {
            newCard = new Card(cardToDeal.cards[0].suit, cardToDeal.cards[0].value, cardToDeal.cards[0].image);
            // console.log(newCard);
            displayHit(newCard);
        })
        delayTimeout();
}

function getShoe(callback) {
    fetch(`https://www.deckofcardsapi.com/api/deck/new/shuffle?deck_count=6`)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
    })
    .then(data => {
        console.log(data);
        callback(data);
    })
}

function findShoe() {
    getShoe(function (decksInfo) {
        deckInfo = decksInfo;
        deckId = decksInfo.deck_id;
        // console.log(deckId);
        document.getElementById('idForDeck').innerText = deckId;
        // console.log(document.getElementById('idForDeck').innerText);
        // dealFourCards(deckId);
        return deckId;
    });
    
}

function startNewHand() {
    clearTable();
    if (isDeckEmpty()) {
        deckId = findShoe();
        dealFourCards(deckId);
    } else {
        dealFourCards(deckId);
        console.log(deckInfo);
    }
    // document.getElementById('Hit-button').disabled = false;
    // document.getElementById('standButton').disabled = false;
}

function drawFourCards(id, callback) {
    console.log(id);
    fetch(`https://www.deckofcardsapi.com/api/deck/${id}/draw?count=4`)
    .then((response) => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        })
        .then(data => {
            callback(data);
        })
        // .then((dealCards) => callback(dealCards));
    }
    
    function dealFourCards(id) {
        drawFourCards(id, function (cardsToDeal) {
            // console.log(cardsToDeal);
            // console.log(Card(cardsToDeal.cards[0].suit, cardsToDeal.cards[0].value));
            let card1 = new Card(cardsToDeal.cards[0].suit, cardsToDeal.cards[0].value, cardsToDeal.cards[0].image);
            displayCard(card1, "playerCard2");
            firstFour.push(card1);
            let card2 = new Card(cardsToDeal.cards[1].suit, cardsToDeal.cards[1].value, cardsToDeal.cards[1].image);
            mysteryCard = card2;
            displayCard(card2, "dealerCard2");
            firstFour.push(card2);
            let card3 = new Card(cardsToDeal.cards[2].suit, cardsToDeal.cards[2].value, cardsToDeal.cards[2].image);
            displayCard(card3, "playerCard1");
            firstFour.push(card3);
            let card4 = new Card(cardsToDeal.cards[3].suit, cardsToDeal.cards[3].value, cardsToDeal.cards[3].image);
            displayCard(card4, "dealerCard1")
            firstFour.push(card4);
            // deal(firstFour);
            console.log(card1);
            console.log(card2);
            console.log(card3);
            console.log(card4);
            console.log(addPlayerValues())
        })
}

var bankroll = JSON.parse(localStorage.getItem("bankroll")) || 2022;

function getBankroll() {
    return bankroll;
}

function setBankroll(bankroll) {
    bankroll = bankroll;
    localStorage.setItem("bankroll", bankroll);

    // bankRoll = newBalance;
}
setBankroll(bankroll);
function displayDollars(dollars) {
    return `$${dollars.toFixed(2)}`;
}
const bettingSection = document.querySelector('#betting');
const playersActions = document.getElementById('playersActions');

function timeToBet() {
//adds class "hideActions" that adds display: none to #playersActions
playersActions.classList.add('hideSection');
bettingSection.classList.add('revealSection');

const bankrollSpan = document.getElementById("bankrollDisplaySpan");
bankrollSpan.innerHTML = `Bankroll: ${displayDollars(getBankroll())}`;
}
// timeToBet();

function timeToPlay() {
    const playersActions = document.getElementById('playersActions');
    playersActions.classList.remove('hideSection');
    bettingSection.classList.remove('revealSection');
    bettingSection.classList.add('hideSection');
    clearTable();
    dealFourCards(deckId);
    console.log(deckId);
    delayTimeout();
    
}
// timeToPlay();

const wagerInputField = document.querySelector('#users-wager');

function getFourCards() {
    const wagerAmt = wagerInputField.value;
    console.log(wagerAmt);
    timeToPlay();
}

function initializeBankRoll() {
    let enteredBankroll = prompt("Please enter a whole number for your bank roll.");
    if(enteredBankroll.length > 0) {
        const bankrollAsNum = Number(enteredBankroll);
        if(Number.isInteger(bankrollAsNum) && bankrollAsNum > 0) {        
            setBankroll(bankrollAsNum)
            timeToBet();
            
        } else {
            alert("Not a valid bank roll. Please try again.");
        }
    } else {
        alert("Not a valid bank roll. Please try again.");
    }
}

let playerDealValue = 0;
let dealerDealValue = 0;
const blackjackValue = 21;

function showDealerCard() {
    let placement = document.getElementById('dealerCard2');
    placement.innerHTML = "";
    let srcStart = "<li data-blackJACK-value='";
    let rankNum = rankToValue(mysteryCard.rank);
        // console.log(rankNum);
        let lengthRank = rankNum.length;
        if(lengthRank > 2 && rankNum != "11/1") {
            rankNum = mysteryCard.rank;
        }
        console.log(rankNum);

        insertTxt = srcStart.concat(rankNum, "' id='card'><img src='", mysteryCard.imgSource, "' alt='", mysteryCard.rank, " of ", mysteryCard.suit, "' height='100px'/></li>"); 
        
        placement.innerHTML +=insertTxt;

}

function displayDealerHit(card) {
    card.suit = suitToWord(card.suit);
    card.rank = rankToWord(card.rank);
    cardValue = rankToValue(card.rank);

    altTxt = card.rank + " of " + card.suit;

    const newCardDealt = document.createElement("li");
    newCardDealt.setAttribute("data-blackJACK-value", cardValue);
    newCardDealt.setAttribute("id", "card");

    const imgSpan = document.createElement("img");
    imgSpan.setAttribute("src", card.imgSource);
    imgSpan.setAttribute("alt", altTxt);
    imgSpan.setAttribute("height", "100px");
    newCardDealt.appendChild(imgSpan);

    document.querySelector("#dealersHand").appendChild(newCardDealt);
    console.log(addDealerValues())
}

function hitDealer() {
    if(!isDeckEmpty()) {
        drawOne(deckId, function (cardToDeal) {
            newCard = new Card(cardToDeal.cards[0].suit, cardToDeal.cards[0].value, cardToDeal.cards[0].image);
            // console.log(newCard);
            displayDealerHit(newCard);
        })
        
    } else {
        alert("No more cards in deck. Please deal a new hand.");
    }
}

function addDealerValues() {
    // document.getElementById('Hit-button').disabled = true;
    // document.getElementById('standButton').disabled = true;

    let handCards = document.querySelector('#dealersCards').querySelectorAll('#card'); 
    let numAces = 0;
    let cardValues = [];
    let handTotal = 0;
    for(let i = 0; i < handCards.length; i++) {
        value = handCards[i].getAttribute('data-blackjack-value');
        if(value == '11/1') {
            numAces++;
        }
        if(value == '?') {
            value = rankToValue(mysteryCard.rank);
            showDealerCard();
        }
        cardValues.push(parseInt(value));
    }
    cardValues.forEach( num => {
        handTotal += num;
    })
    
    for(i = 0; i < numAces; i++) {
        if(handTotal > blackjackValue) {
            handTotal -= 10;
        }
    }

    if(handTotal < 17) {
        hitDealer();
    }
    console.log(handTotal);
    if(handTotal > blackjackValue) {
        console.log('dealer busted');
    }
    return handTotal;
}

function addPlayerValues() {
    let handCards = document.querySelector('#playersCards').querySelectorAll('#card'); 
    let numAces = 0;
    let cardValues = [];
    let handTotal = 0;
    for(let i = 0; i < handCards.length; i++) {
        value = handCards[i].getAttribute('data-blackjack-value');
        if(value == '11/1') {
            numAces++;
            console.log("added ace of " + value)
        }
        cardValues.push(parseInt(value));
    }
    cardValues.forEach( num => {
        handTotal += num;
    })
    console.log("Player's hand, aces high: " + handTotal);
    
    for(i = 0; i < numAces; i++) {
        if(handTotal > blackjackValue) {
            handTotal -= 10;
            console.log("minus 10");
        }
    }
    console.log("Player's hand, aces adjusted: " + handTotal);
    if(handTotal > blackjackValue) {
        console.log('player busted');
        
        addDealerValues();
    }
    return handTotal;
    
}



// dealFourCards();

document.getElementById('WagerButton').addEventListener('click', getFourCards);
document.getElementById('bankrollSetup').addEventListener("click", initializeBankRoll);

document.getElementById('standButton').addEventListener('click', addDealerValues);

document.getElementById('Hit-button').addEventListener("click", getOneCard);
// document.getElementById('hit-button').addEventListener("click", addPlayerCard);

document.getElementById('deal').addEventListener("click", timeToPlay);